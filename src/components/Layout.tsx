import React, { ReactNode } from "react";

interface LayoutProps {
  children: ReactNode;
  isCoverActive?: boolean;
}

const Layout: React.FC<LayoutProps> = ({ children, isCoverActive }) => {
  return (
    <div className="layout">
      <header>
        <h1>Event App</h1>
      </header>
      <main>
        {isCoverActive && <div className="cover-active" />}
        {children}
      </main>
      <footer>&copy; 2024 Event App</footer>
    </div>
  );
};

export default Layout;
