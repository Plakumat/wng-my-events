import React from "react";

// components
import Link from "next/link";

// styles
import styled from "styled-components";

// services
import { EventData } from "../../pages/api/events";

interface PaymentFormProps {
  onSubmit: (e: React.FormEvent) => void;
  reservationSnippet: React.ReactElement;
  eventData: EventData;
  requestSuccess: boolean;
}

const PaymentForm: React.FC<PaymentFormProps> = ({ onSubmit, reservationSnippet, eventData, requestSuccess }) => {
  return (
    <>
      <Link href={`/events/${eventData.id}`}>
        <BackToEvent>Back to {eventData.title} Page</BackToEvent>
      </Link>
      <FormContainer onSubmit={onSubmit}>
        {reservationSnippet}
        <FormGroup>
          <label htmlFor="cardNumber">Card Number:</label>
          <input type="text" id="cardNumber" name="cardNumber" required />
        </FormGroup>
        <FormGroup>
          <label htmlFor="expirationDate">Expiration Date:</label>
          <input type="text" id="expirationDate" name="expirationDate" required />
        </FormGroup>
        <FormGroup>
          <label htmlFor="cvv">CVV:</label>
          <input type="text" id="cvv" name="cvv" required />
        </FormGroup>
        <FormGroup>
          <label htmlFor="amount">Amount:</label>
          <input type="text" id="amount" name="amount" value={eventData.price + " $"} disabled />
        </FormGroup>
        <SubmitButton type="submit" disabled={requestSuccess}>
          Submit Payment
        </SubmitButton>
      </FormContainer>
    </>
  );
};

export default PaymentForm;

const FormContainer = styled.form`
  display: flex;
  flex-direction: column;
  gap: var(--margin-20);
  padding: var(--margin-20);
  width: 100%;
  max-width: 600px;
  margin: var(--margin-20) 0 0 0;
  border-radius: var(--margin-8);
  background-color: var(--white-color);
`;

const FormGroup = styled.div`
  display: flex;
  flex-direction: column;
  gap: var(--margin-10);

  label {
    font-weight: bold;
  }

  input {
    padding: var(--margin-8);
    border: 1px solid #ccc;
    border-radius: 4px;
  }
`;

const SubmitButton = styled.button`
  padding: var(--margin-10);
  background-color: #007bff;
  font-size: var(--font-size-16);
  color: var(--white-color);
  border: none;
  border-radius: 4px;
  cursor: pointer;
  transition: background-color 0.3s ease;

  &:hover {
    background-color: #0056b3;
  }

  &:disabled {
    background-color: var(--light-gray-color);

    cursor: default;
  }
`;

const BackToEvent = styled.button`
  position: absolute;
  top: var(--margin-10);
  left: var(--margin-10);
  padding: var(--margin-10) var(--margin-20);
  border: none;
  background-color: var(--blue-color);
  color: var(--white-color);
  font-size: var(--font-size-16);
  text-decoration: none;
  border-radius: 5px;
  cursor: pointer;

  @media (max-width: 768px) {
    position: relative;
    top: 0;
    left: 0;
  }
`;
