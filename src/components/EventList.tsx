import React, { Suspense, useEffect, useState } from "react";

// styles
import styled from "styled-components";

// components
import EventCardSkeleton from "../../src/components/EventCardSkeleton";
import Pagination from "./Pagination";

// model
import { EventData } from "../../pages/api/events";

const LazyEventCard = React.lazy(async () => {
  await new Promise((resolve) => setTimeout(resolve, 1500));
  return import("./EventCard");
});

const EventList: React.FC = () => {
  const [events, setEvents] = useState<EventData[]>([]);
  const [page, setPage] = useState(1);
  const [hasMore, setHasMore] = useState(true);

  // fetch event by page number
  const fetchEvents = async (pageNumber: number) => {
    try {
      const response = await fetch(`/api/events?page=${pageNumber}&pageSize=10`);
      const newEvents = await response.json();

      setEvents(newEvents);
      setPage(pageNumber);
      setHasMore(newEvents.length === 10);
    } catch (error) {
      console.error("An error occurred while retrieving events:", error);
    }
  };

  // if page change get events
  const handlePageChange = (newPage: number) => {
    fetchEvents(newPage);
  };

  useEffect(() => {
    const fetchInitialEvents = async () => {
      try {
        await fetchEvents(1);
      } catch (error) {
        console.error("An error occurred while retrieving first events:", error);
      }
    };

    fetchInitialEvents();
  }, []);

  return (
    <EventListContainer>
      <EventCardContainer>
        {events.map((event) => (
          <Suspense key={event.id} fallback={<EventCardSkeleton />}>
            <LazyEventCard key={event.id} {...event} />
          </Suspense>
        ))}
      </EventCardContainer>
      <Pagination
        currentPage={page}
        hasNextPage={hasMore}
        onPrevPage={() => handlePageChange(page - 1)}
        onNextPage={() => handlePageChange(page + 1)}
      />
    </EventListContainer>
  );
};

export default EventList;

const EventListContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: auto;
`;

const EventCardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
`;
