import React, { useEffect, useState } from "react";

// components
import Image from "next/image";
import Link from "next/link";

// styles
import styled from "styled-components";

// model
import { EventData } from "../../pages/api/events";

const EventCard: React.FC<EventData> = ({ id, title, date, time, location, description, availableSeats, price, image, category }) => {
  const [hasBeenReserved, setHasBeenReserved] = useState<boolean>(false);

  useEffect(() => {
    // if the current event reserved before apply specified DOM manuplations
    if (localStorage.getItem("reservedEvents")) {
      const reservedEvents = JSON.parse(localStorage.getItem("reservedEvents")!);
      const currentId: any = id;
      reservedEvents.includes(parseInt(currentId)) ? setHasBeenReserved(true) : setHasBeenReserved(false);
    }
  }, [id]);

  return (
    <EventCardContainer>
      <EventUrl href={`/events/${id}`} />
      <EventCardImage>
        <Image src={`${image}?${Math.random()}`} alt={title} layout="fill" objectFit="cover" />
      </EventCardImage>
      <EventCardContent>
        <EventCardTitle>{title}</EventCardTitle>
        <EventCardDate>
          {date} at {time}
        </EventCardDate>
        <EventCardLocation>{location}</EventCardLocation>
        <EventAvailableSeats>{availableSeats} Seats Available</EventAvailableSeats>
        <EventCardDescription>{description}</EventCardDescription>
        <EventCategory>{category}</EventCategory>
        <EventPrice>{price} $</EventPrice>
      </EventCardContent>
      {hasBeenReserved ? <EventReserveInfo>You reserved.</EventReserveInfo> : <></>}
    </EventCardContainer>
  );
};

export default EventCard;

const EventCardContainer = styled.div`
  position: relative;
  width: calc((100% / 5) - var(--margin-20));
  min-height: 500px;
  margin: var(--margin-20) var(--margin-10);
  border: 1px solid var(--nearly-white-color);
  border-radius: var(--margin-8);
  overflow: hidden;
  box-shadow: 0 0 var(--margin-10) var(--shadow-color);
  transition: transform 0.3s ease;

  &:hover {
    transform: scale(1.05);
  }

  & a {
    text-decoration: none;
  }

  @media (max-width: 1200px) {
    width: calc((100% / 4) - var(--margin-20));
  }

  @media (max-width: 1050px) {
    width: calc((100% / 3) - var(--margin-20));
  }

  @media (max-width: 840px) {
    width: calc((100% / 2) - var(--margin-20));
  }

  @media (max-width: 540px) {
    width: 100%;
  }
`;

const EventCardImage = styled.div`
  position: relative;
  width: 100%;
  height: 200px;
  overflow: hidden;
  border-bottom: 1px solid var(--nearly-white-color);
  border-radius: 8px 8px 0 0;
`;

const EventCardContent = styled.div`
  padding: var(--margin-20);
`;

const EventCardTitle = styled.h3`
  font-size: var(--font-size-24);
  margin-bottom: var(--margin-10);
`;

const EventCardDate = styled.p`
  color: var(--dark-gray-color);
  font-size: var(--font-size-14);
  margin-bottom: 5px;
`;

const EventAvailableSeats = styled.p`
  color: var(--dark-gray-color);
  font-size: var(--font-size-14);
  margin-bottom: 5px;
`;

const EventPrice = styled.p`
  margin: 5px 0 0 0;
  color: var(--dark-gray-color);
  font-size: var(--font-size-16);
  font-weight: bold;
  margin-bottom: 5px;
`;

const EventCategory = styled.p`
  color: var(--dark-gray-color);
  font-size: var(--font-size-14);
`;

const EventCardLocation = styled.p`
  font-size: var(--font-size-14);
  margin-bottom: 5px;
`;

const EventCardDescription = styled.p`
  color: var(--dark-gray-color);
  font-size: var(--font-size-14);
`;

const EventReserveInfo = styled.span`
  position: absolute;
  bottom: 15px;
  left: 50%;
  padding: 0 var(--margin-20);
  width: 100%;
  color: var(--green-color);
  font-size: var(--font-size-14);
  font-weight: bold;
  transform: translateX(-50%);
`;

const EventUrl = styled(Link)`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;
