// styles
import styled, { keyframes } from "styled-components";

const EventCardSkeleton = () => {
  return (
    <SkeletonContainer>
      <SkeletonImage />
      <SkeletonContent>
        <SkeletonTitle />
        <SkeletonDate />
        <SkeletonInfo />
        <SkeletonDescription />
        <SkeletonReserveInfo />
      </SkeletonContent>
    </SkeletonContainer>
  );
};

export default EventCardSkeleton;

const skeletonAnimation = keyframes`
  0% {
    background-position: -200px 0;
  }
  100% {
    background-position: 200px 0;
  }
`;

const SkeletonContainer = styled.div`
  position: relative;
  width: calc((100% / 5) - var(--margin-20));
  min-height: 500px;
  margin: var(--margin-20) var(--margin-10);
  border: 1px solid var(--nearly-white-color);
  border-radius: var(--margin-8);
  overflow: hidden;
  box-shadow: 0 0 var(--margin-10) var(--shadow-color);
  animation: ${skeletonAnimation} 1.5s linear infinite;

  @media (max-width: 1200px) {
    width: calc((100% / 4) - var(--margin-20));
  }

  @media (max-width: 1050px) {
    width: calc((100% / 3) - var(--margin-20));
  }

  @media (max-width: 840px) {
    width: calc((100% / 2) - var(--margin-20));
  }

  @media (max-width: 540px) {
    width: 100%;
  }
`;

const SkeletonImage = styled.div`
  position: relative;
  width: 100%;
  height: 200px;
  overflow: hidden;
  border-bottom: 1px solid var(--nearly-white-color);
  background: linear-gradient(90deg, var(--skeleton-light-color) 25%, var(--skeleton-dark-color) 50%, var(--skeleton-light-color) 75%);
  background-size: 400% 100%;
  animation: ${skeletonAnimation} 1.5s linear infinite;

  & > div {
    display: none !important;
  }
`;

const SkeletonContent = styled.div`
  padding: var(--margin-20);
`;

const SkeletonTitle = styled.div`
  width: 70%;
  height: var(--margin-20);
  margin-bottom: var(--margin-10);
  background: linear-gradient(90deg, var(--skeleton-light-color) 25%, var(--skeleton-dark-color) 50%, var(--skeleton-light-color) 75%);
  background-size: 400% 100%;
  animation: ${skeletonAnimation} 1.5s linear infinite;
`;

const SkeletonDate = styled.div`
  width: 40%;
  height: 12px;
  margin-bottom: 5px;
  background: linear-gradient(90deg, var(--skeleton-light-color) 25%, var(--skeleton-dark-color) 50%, var(--skeleton-light-color) 75%);
  background-size: 400% 100%;
  animation: ${skeletonAnimation} 1.5s linear infinite;
`;

const SkeletonInfo = styled.div`
  width: 60%;
  height: 12px;
  margin-bottom: 5px;
  background: linear-gradient(90deg, var(--skeleton-light-color) 25%, var(--skeleton-dark-color) 50%, var(--skeleton-light-color) 75%);
  background-size: 400% 100%;
  animation: ${skeletonAnimation} 1.5s linear infinite;
`;

const SkeletonDescription = styled.div`
  width: 100%;
  height: var(--font-size-16);
  background: linear-gradient(90deg, var(--skeleton-light-color) 25%, var(--skeleton-dark-color) 50%, var(--skeleton-light-color) 75%);
  background-size: 400% 100%;
  animation: ${skeletonAnimation} 1.5s linear infinite;
`;

const SkeletonReserveInfo = styled.div`
  position: absolute;
  bottom: 15px;
  width: 40%;
  height: var(--margin-20);
  background: linear-gradient(90deg, var(--green-color) 25%, var(--skeleton-green-light-color) 50%, var(--green-color) 75%);
  background-size: 400% 100%;
  animation: ${skeletonAnimation} 1.5s linear infinite;
`;
