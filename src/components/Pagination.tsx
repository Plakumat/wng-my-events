import React from "react";

// styles
import styled from "styled-components";

interface PaginationProps {
  currentPage: number;
  hasNextPage: boolean;
  onPrevPage: () => void;
  onNextPage: () => void;
}

const Pagination: React.FC<PaginationProps> = ({ currentPage, hasNextPage, onPrevPage, onNextPage }) => {
  return (
    <PaginationContainer>
      <PageButton onClick={onPrevPage} disabled={currentPage === 1}>
        Previous Page
      </PageButton>
      <span style={{ margin: "0 var(--margin-10)" }}>{currentPage}</span>
      <PageButton onClick={onNextPage} disabled={!hasNextPage}>
        Next Page
      </PageButton>
    </PaginationContainer>
  );
};

export default Pagination;

const PaginationContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: var(--margin-20);
`;

const PageButton = styled.button`
  background-color: var(--green-color);
  color: var(--white-color);
  font-size: var(--font-size-16);
  padding: var(--margin-10) 15px;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  margin: 0 var(--margin-10);

  &:disabled {
    background-color: var(--light-gray-color);

    cursor: default;
  }
`;
