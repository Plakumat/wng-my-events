// pages/_app.tsx
import { AppProps } from "next/app";
import Head from "next/head";

// style
import "../src/styles/style.scss";

const MyApp = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <Head>
        <title>Event App</title>
      </Head>
      <Component {...pageProps} />
    </>
  );
};

export default MyApp;
