import { useEffect, useState } from "react";

// next components & props
import { NextPage } from "next";
import Link from "next/link";
import { useRouter } from "next/router";

// styles
import styled from "styled-components";

// components
import Layout from "../../src/components/Layout";

// services
import { EventData, getEventById } from "../api/events";

interface EventDetailPageProps {
  initialEvent?: EventData;
}

const EventDetailPage: NextPage<EventDetailPageProps> = ({ initialEvent }) => {
  const router = useRouter();
  const { id } = router.query;
  const [event, setEvent] = useState<EventData | null>(initialEvent || null);
  const [hasBeenReserved, setHasBeenReserved] = useState<boolean>(false);

  // reservation action handle
  const handleReservationClick = () => {
    if (id) {
      router.push(`/reservations/${id}`);
    }
  };

  useEffect(() => {
    // get all events from start
    const fetchEvent = async () => {
      if (id && !event) {
        const fetchedEvent = await getEventById(id as string);
        // if there's no event data redirect 404
        fetchedEvent ? setEvent(fetchedEvent) : router.replace("/404");
      }
    };

    fetchEvent();
  }, [id, event, router]);

  useEffect(() => {
    // detect reserved events and mark them actions
    if (localStorage.getItem("reservedEvents")) {
      const reservedEvents = JSON.parse(localStorage.getItem("reservedEvents")!);
      const currentId: any = id;
      reservedEvents.includes(parseInt(currentId)) ? setHasBeenReserved(true) : setHasBeenReserved(false);
    }
  }, [id]);

  // if there's no event data redirect 404 null check
  if (!event) {
    return null;
  }

  return (
    <Layout isCoverActive={true}>
      <EventDetailContainer>
        <Link href="/">
          <BackButton>Back to Events</BackButton>
        </Link>
        <EventImageWrapper>
          <EventImage src={event.image} alt={event.title} />
        </EventImageWrapper>
        <EventTitle>{event.title}</EventTitle>
        {hasBeenReserved ? <EventReserveInfo>You have booked this event.</EventReserveInfo> : <></>}
        <EventInfo>Date: {event.date}</EventInfo>
        <EventInfo>Time: {event.time}</EventInfo>
        <EventInfo>Location: {event.location}</EventInfo>
        <EventDescription>{event.description}</EventDescription>
        {event.additionalInfo && <EventDescription>Additional Info: {event.additionalInfo}</EventDescription>}
        <EventPrice>{event.price} $</EventPrice>
        <ReservationButton onClick={handleReservationClick} disabled={hasBeenReserved}>
          Make Reservation
        </ReservationButton>
      </EventDetailContainer>
    </Layout>
  );
};

export default EventDetailPage;

const EventDetailContainer = styled.div`
  width: 100%;
  max-width: 800px;
  max-height: 766px;
  margin: var(--margin-45) auto;
  padding: var(--margin-20);
  background-color: var(--white-color);
  border-radius: var(--margin-8);
  box-shadow: 0 0 var(--margin-10) var(--shadow-color);
  z-index: 10;
`;

const BackButton = styled.button`
  position: absolute;
  top: var(--margin-10);
  left: var(--margin-20);
  padding: var(--margin-10) var(--margin-20);
  border: none;
  background-color: var(--blue-color);
  color: var(--white-color);
  font-size: var(--font-size-16);
  text-decoration: none;
  border-radius: 5px;
  cursor: pointer;
`;

const EventTitle = styled.h1`
  padding: 0;
  margin: 0;
  font-size: var(--font-size-24);
  color: var(--dark-gray-color);
`;

const EventInfo = styled.p`
  padding: 0;
  margin: 0;
  color: var(--dark-gray-color);
  font-size: var(--font-size-14);
  margin-bottom: var(--margin-8);
`;

const EventDescription = styled.p`
  padding: 0;
  margin: 0;
  color: var(--dark-gray-color);
  font-size: var(--font-size-14);
  line-height: 1.6;
  margin-bottom: var(--margin-8);
`;

const EventPrice = styled.p`
  padding: 0;
  margin: 0;
  color: var(--dark-gray-color);
  font-size: var(--font-size-16);
  font-weight: bold;
  line-height: 1.6;
  margin-bottom: var(--margin-8);
`;

const ReservationButton = styled.button`
  background-color: var(--green-color);
  color: var(--white-color);
  padding: var(--margin-10) var(--margin-20);
  border: none;
  border-radius: 5px;
  cursor: pointer;
  font-size: var(--font-size-16);
  margin: var(--margin-10) 0 0 0;

  &:disabled {
    background-color: var(--light-gray-color);
    cursor: default;
  }
`;

const EventImageWrapper = styled.div`
  width: 100%;
  max-height: 400px;
  border-radius: var(--margin-8);
  margin-bottom: var(--margin-20);
`;

const EventImage = styled.img`
  width: 100%;
  max-height: 400px;
  object-fit: cover;
  border-radius: var(--margin-8);
`;

const EventReserveInfo = styled.p`
  margin: var(--margin-10) 0;
  color: var(--green-color);
  font-size: var(--font-size-14);
  font-weight: bold;
`;
