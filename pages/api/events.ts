import { NextApiRequest, NextApiResponse } from "next";

// logger

// data
import eventsData from "../../public/data/events.json";

export interface EventData {
  id: number;
  title: string;
  date: string;
  time: string;
  location: string;
  description: string;
  availableSeats: number;
  price: string;
  image: string;
  category: string;
  additionalInfo?: string;
  detail: any;
}

// create a new instance of pino for logger
const logger = require("pino")();

// random time generator for mock json data
export const generateRandomTime = () => {
  const hours = String(Math.floor(Math.random() * 24)).padStart(2, "0");
  const minutes = String(Math.floor(Math.random() * 60)).padStart(2, "0");
  return `${hours}:${minutes}`;
};

// get event ids server action
export const getEventIds = (): string[] => {
  try {
    logger.info("GetEventIds");
    logger.info({ message: "getEventIds function called:" });
    return eventsData.events.map((event) => String(event.id));
  } catch (error) {
    logger.error("GetEventIds");
    logger.error({ message: "Error in getEventIds:", error });
    return [];
  }
};

// get event by id server action
export const getEventById = (id: string): EventData | null => {
  try {
    logger.info("GetEventById");
    logger.info({ message: "getEventById function called:" });
    const selectedEvent: any = eventsData.events.find((event) => String(event.id) === id) || null;
    return selectedEvent;
  } catch (error) {
    logger.error("GetEventById");
    logger.error({ message: "Error in getEventById:", error });
    return null;
  }
};

// main handler of api/events endpoint
export default function handler(req: NextApiRequest, res: NextApiResponse) {
  // get page params from client
  const page = parseInt(req.query.page as string, 10) || 1;
  const pageSize = parseInt(req.query.pageSize as string, 10) || 10;

  try {
    const startIdx = (page - 1) * pageSize;
    const endIdx = startIdx + pageSize;

    logger.info("Handler retrieving data:");
    logger.info({
      message: "API handler function called",
      method: req.method,
      url: req.url,
      params: req.query,
      body: req.body,
    });
    const events = eventsData.events.slice(startIdx, endIdx);

    res.status(200).json(events);
  } catch (error) {
    logger.error("Error retrieving data:", error);
    logger.error({
      message: "Error in API handler",
      error,
      method: req.method,
      url: req.url,
      params: req.query,
      body: req.body,
    });
    res.status(500).json({ error: "Internal Server Error" });
  }
}
