import { NextApiRequest, NextApiResponse } from "next";

// main handler of api/reservation endpoint
export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  if (req.method === "POST") {
    try {
      await new Promise((resolve) => setTimeout(resolve, 1000));
      const success = true;

      // create a simulation of success response to pass the payment step
      if (success) {
        const eventId = req.body.eventId;

        res.status(200).json({ success: true, reservedId: eventId });
      } else {
        res.status(400).json({ success: false, error: "Reservation failed." });
      }
    } catch (error) {
      console.error("Reservation error:", error);
      res.status(500).json({ success: false, error: "Internal server error." });
    }
  } else {
    res.status(405).json({ error: "Method not allowed." });
  }
}
