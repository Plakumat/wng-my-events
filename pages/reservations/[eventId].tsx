import { useRouter } from "next/router";
import { useEffect, useState } from "react";

// components
import Link from "next/link";
import Layout from "../../src/components/Layout";
import PaymentForm from "../../src/components/PaymentForm";

// styles
import styled from "styled-components";

// services
import { EventData, getEventById } from "../api/events";

interface ReservationPageProps {
  initialEvent?: EventData;
}

interface ProgressBarProps {
  progress: number;
}

const ReservationPage: React.FC<ReservationPageProps> = ({ initialEvent }) => {
  const router = useRouter();
  const { eventId } = router.query;
  const [event, setEvent] = useState<EventData | null>(initialEvent || null);
  const [reservationComplete, setReservationComplete] = useState(false);
  const [progress, setProgress] = useState(0);
  const [remainingTime, setRemainingTime] = useState(900);
  const [isRequestSuccess, setIsRequestSuccess] = useState<boolean>(false);
  const [hasBeenReserved, setHasBeenReserved] = useState<boolean>(false);

  const redirectToHome = () => {
    setTimeout(() => {
      router.push(`/`);
    }, 1500);
  };

  // reservation action handle
  const handleReservationSubmit = (e: React.FormEvent) => {
    e.preventDefault();

    // set true for show payment step
    setReservationComplete(true);
  };

  const handlePaymentSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      const eventId = event?.id;

      // get current id & complete payment step
      const response = await fetch("/api/reservation", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ eventId }),
      });

      const data = await response.json();

      if (data.success) {
        // if there's stored data in ls concat actions
        const reservedEvents = JSON.parse(localStorage.getItem("reservedEvents")!) || [];

        if (!reservedEvents.includes(eventId)) {
          const updatedReservedEvents = [...reservedEvents, eventId];
          localStorage.setItem("reservedEvents", JSON.stringify(updatedReservedEvents));
        }

        alert("Reservation completed successfully! You are directing to the event list page.");
        setIsRequestSuccess(true);

        // wait 1.5 second and redirect to root after if payment success
        redirectToHome();
      } else {
        alert("Reservation could not be made. Error: " + data.error);
        setIsRequestSuccess(false);
      }
    } catch (error) {
      console.error("An error occurred during the booking process:", error);
      setIsRequestSuccess(false);
      alert("An error occurred during the booking process.");
    }
  };

  const reservationInfoSnippet = (event: EventData) => {
    return (
      <ReservationInfo>
        <p>
          <strong>Title:</strong> {event.title}
        </p>
        <p>
          <strong>Date:</strong> {event.date}
        </p>
        <p>
          <strong>Time:</strong> {event.time}
        </p>
        <p>
          <strong>Price:</strong> {event.price} $
        </p>
        <p>
          <strong>Location:</strong> {event.location}
        </p>
        <p>
          <strong>Description:</strong> {event.description}
        </p>
        <p>
          <strong>Category:</strong> {event.category}
        </p>
        <p>
          <strong>Available Seats:</strong> {event.availableSeats}
        </p>
      </ReservationInfo>
    );
  };

  // minute display method
  const formatTime = (time: number) => {
    const minutes = Math.floor(time / 60);
    const seconds = time % 60;
    return `${minutes}:${seconds < 10 ? `0${seconds}` : seconds}`;
  };

  useEffect(() => {
    const fetchEvent = async () => {
      if (eventId && !event) {
        const fetchedEvent = await getEventById(eventId as string);
        // if there's no event data redirect 404
        fetchedEvent ? setEvent(fetchedEvent) : router.replace("/404");
      }
    };

    fetchEvent();
  }, [eventId, event, router]);

  useEffect(() => {
    // if the current event reserved before apply specified DOM manuplations
    if (localStorage.getItem("reservedEvents")) {
      const reservedEvents = JSON.parse(localStorage.getItem("reservedEvents")!);
      const currentId: any = eventId;
      reservedEvents.includes(parseInt(currentId)) ? setHasBeenReserved(true) : setHasBeenReserved(false);
    }
  }, [eventId]);

  useEffect(() => {
    if (remainingTime === 0) {
      redirectToHome();
    }
  }, [remainingTime]);

  useEffect(() => {
    // detech refresh action for prevent stored info that filled by user
    const handleBeforeUnload = (event: BeforeUnloadEvent) => {
      const confirmationMessage = "Are you sure you want to refresh the page? All your changes will be lost.";
      event.returnValue = confirmationMessage;
      return confirmationMessage;
    };

    window.addEventListener("beforeunload", handleBeforeUnload);

    return () => {
      window.removeEventListener("beforeunload", handleBeforeUnload);
    };
  }, []);

  useEffect(() => {
    // 15 minutes interval setter
    const interval = setInterval(() => {
      setProgress((prevProgress) => (prevProgress < 100 ? prevProgress + 100 / 900 : 100));
      setRemainingTime((prevTime) => (prevTime > 0 ? prevTime - 1 : 0));
    }, 1000);

    return () => clearInterval(interval);
  }, []);

  if (!event) {
    return null;
  }

  // if current root reserved before redirect to events list page
  if (hasBeenReserved) {
    router.push(`/events/${eventId}`);
  }

  return (
    <Layout>
      <h2>Make a {reservationComplete ? "Payment" : "Reservation"}</h2>
      {!isRequestSuccess ? (
        <PopupContainer>
          <p>
            {progress < 100
              ? `You must make your reservation and payment within 15 minutes. Remaining time: ${formatTime(remainingTime)}`
              : "Time is up, you will be redirected to the events page."}
          </p>
          <ProgressContainer>
            <ProgressFiller progress={progress} />
          </ProgressContainer>
        </PopupContainer>
      ) : (
        <></>
      )}
      {reservationComplete ? (
        <PaymentForm
          eventData={event}
          onSubmit={handlePaymentSubmit}
          reservationSnippet={reservationInfoSnippet(event)}
          requestSuccess={isRequestSuccess}
        />
      ) : (
        <>
          <Link href={`/events/${event.id}`}>
            <BackToEvent>Back to {event.title} Page</BackToEvent>
          </Link>
          <ReservationForm onSubmit={handleReservationSubmit}>
            {reservationInfoSnippet(event)}
            <ReservationFields>
              <label htmlFor="name">Name:</label>
              <input type="text" id="name" name="name" required />

              <label htmlFor="email">Email:</label>
              <input type="email" id="email" name="email" required />
            </ReservationFields>
            <SubmitButton type="submit">Submit Reservation</SubmitButton>
          </ReservationForm>
        </>
      )}
    </Layout>
  );
};

export default ReservationPage;

const ReservationForm = styled.form`
  display: flex;
  flex-direction: column;
  gap: var(--margin-20);
  padding: var(--margin-20);
  width: 100%;
  max-width: 600px;
  margin: auto;
  border-radius: var(--margin-8);
  background-color: var(--white-color);
`;

const ReservationInfo = styled.div`
  background-color: var(--light-gray-bg-color);
  padding: var(--margin-20);
  border-radius: var(--margin-8);
  box-shadow: 0 0 var(--margin-10) var(--shadow-color);

  h3 {
    font-size: 1.5rem;
    margin-bottom: var(--margin-10);
  }

  p {
    margin: 0;
    margin-bottom: var(--margin-8);

    strong {
      font-weight: bold;
    }
  }
`;

const ReservationFields = styled.div`
  display: flex;
  flex-direction: column;
  gap: var(--margin-10);

  label {
    font-weight: bold;
  }

  input {
    padding: var(--margin-8);
    border: 1px solid #ccc;
    border-radius: 4px;
  }
`;

const SubmitButton = styled.button`
  font-size: var(--font-size-16);
  padding: var(--margin-10);
  background-color: #007bff;
  color: var(--white-color);
  border: none;
  border-radius: 4px;
  cursor: pointer;
  transition: background-color 0.3s ease;

  &:hover {
    background-color: #0056b3;
  }
`;

const BackToEvent = styled.button`
  position: absolute;
  top: var(--margin-10);
  left: var(--margin-10);
  padding: var(--margin-10) var(--margin-20);
  border: none;
  background-color: var(--blue-color);
  color: var(--white-color);
  font-size: var(--font-size-16);
  text-decoration: none;
  border-radius: 5px;
  cursor: pointer;

  @media (max-width: 768px) {
    position: relative;
    top: 0;
    left: 0;
  }
`;

const PopupContainer = styled.div`
  position: fixed;
  top: var(--margin-20);
  right: var(--margin-10);
  width: 300px;
  background-color: var(--white-color);
  box-shadow: 0 0 var(--margin-10) rgba(0, 0, 0, 0.2);
  border-radius: 5px;
  padding: var(--margin-10);
  display: flex;
  flex-direction: column;
  align-items: center;
  z-index: 9999;

  @media (max-width: 768px) {
    position: relative;
    top: 0;
    right: 0;
    margin: var(--margin-20) 0;
    width: 100%;
    max-width: 600px;
  }
`;

const ProgressContainer = styled.div`
  width: 100%;
  height: 30px;
  background-color: var(--nearly-white-color);
  border-radius: 5px;
  margin-top: var(--margin-10);
`;

const ProgressFiller = styled.div<ProgressBarProps>`
  height: 100%;
  width: ${({ progress }) => `${progress}%`};
  background-color: var(--green-color);
  border-radius: 5px;
`;
