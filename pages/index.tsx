import React, { lazy, Suspense } from "react";

// components
import Layout from "../src/components/Layout";

const LazyEventList = lazy(() => import("../src/components/EventList"));

const Home: React.FC = () => {
  return (
    <>
      <Layout>
        <Suspense>
          <LazyEventList />
        </Suspense>
      </Layout>
    </>
  );
};

export default Home;
