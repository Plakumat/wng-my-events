## My Events - Introduction

Live Preview: [https://wng-my-events.vercel.app/](https://wng-my-events.vercel.app/)

My events project is a mock data application made to list cultural events.

Node Version should be v18.17.0 to get the best efficiency.

## Tech Tools

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

Libraries used:

- Typescript
- Next.js
- React Hooks
- Styled Components
- Sass
- Pino

## File & Folder Structure

```
├── pages/
│   ├── api
│   │    ├── events.ts
│   │    └── reservation.ts
│   │
│   ├── events
│   │    └── [id].tsx
│   │
│   ├── reservations
│   │    └── [eventId].tsx
│   │
│   ├── _app.tsx
│   │
│   ├── index.tsx
│   │
├── public/
│   ├── data
│   │    └── events.json
│   │
│   │
├── src/
│   ├── components
│   │       ├── EventCard
│   │       ├── EventCardSkeleton
│   │       ├── EventList
│   │       ├── Layout
│   │       ├── Pagination
│   │       └── PaymentForm
│   │
│   └── styles
│           ├── style.scss
│           └── variables.scss
│
├── .eslintrc.json
├── .gitignore
├── next-env.d.ts
├── next.config.js
├── package.json
├── README.md
├── tsconfig.json
└── yarn.lock
```

## Available Scripts

### `yarn dev`

### `yarn build`

### `yarn start`

### `yarn lint`
